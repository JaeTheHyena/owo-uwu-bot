# OwO uwu bot

The worst Discord Bot ever.  
Created for the Discord hacking week 2019, made using Vim with <3  
Feel free to clone it, modify it and share it and do what you want with the source.  
Also, this bot contains spoilers about endgame.  
  
This bot is made to count user's swears and do stats with it.

Invite the [Official Demo](https://discordapp.com/oauth2/authorize?client_id=591526073515245581&scope=bot&permissions=2112) to your server.

## Dependencies
 - [NodeJs](https://nodejs.org/en/)
 - [Discord.JS](https://discord.js.org/)
 - [Perspective API](http://www.perspectiveapi.com)
 - [Math-random](https://github.com/michaelrhodes/math-random)
 - [Quick.db](https://www.npmjs.com/package/quick.db)

## How to use

Clone the repo:
```
git clone https://gitlab.com/JaeTheHyena/owo-uwu-bot.git
```

Install dependencies:
```
cd owo-uwu-bot
npm i
```

Add your bot token to `config.json`.

Run the bot:
```
node index.js
```
