/*
 * This bot was made for the Discord 'hacking week' 2019.
 * His main goal is to detect insults and respon to them with messages.
 * Original bot by Jaekr <jae@jaekr.me> http://jaekr.me
 * If you want to take the source code to make your own version, feel free to but please at least link this repo.
 * Thank you.
 *
 */

const Discord = require('discord.js'); // Discord.js library
const client = new Discord.Client(); // Create the discord client
var request = require('request'); // Request module
var random = require('math-random') // Real random generator

const config = require('./config'); // Import the config
const start = require('./startup.json'); // Import startup lines

var db = require('quick.db'); // Database support

client.on('ready', () => { // When client is started up and ready
    let lineNumber = Math.floor(random() * 11); // Get a random line from startup.json
    console.log(start[lineNumber]); // Tells a random line

    console.log(`[logged as ${client.user.tag}]`); // Tells what is the bot logged as

    testPresence();
});

function testPresence() { // Function that runs every 10 seconds to update the presence
    let gcount = db.fetch(`globalSwear`);

    client.user.setPresence({ // Bot presence
        game: {
            name: `Doing stats. ${gcount} swears. Watching ${client.guilds.size} servers and over ${client.users.size} users.`,
            type: "STREAMING",
            url: "https://www.twitch.tv/jaethehyena"
        }
    });
    setTimeout(arguments.callee, 10000);
}

client.on('message', async msg => { // On client message

    if (msg.author.bot) return; // Check if author is a bot.

    var msgCont = msg.content; // Get message content

    db.add(`msgCount`, 1); // Number of all messages

    // Worst code ever
    var options = {
        uri: `https://commentanalyzer.googleapis.com/v1alpha1/comments:analyze?key=${config.perspect}`, // URL of the API
        method: 'POST', // Method to post
        body: {
            comment: { text: msgCont }, // We send the message content
            languages: ['en'], // We only want one language at the time because i am too lazy to add more.
            requestedAttributes: { TOXICITY: {} }, // Gets only toxicity
            doNotStore: true // Tells google to not store our data (I think they will not :] )
        },
        json: true // We are sending Json
    };

    request(options, function(error, response, body) { // Actual request function

        try {
            if (body.attributeScores.TOXICITY.summaryScore.value > .70) { // If the score of the message is greater than 0.70, this is considered as malicious.

                let userID = msg.author.id;

                let swears = db.fetch(`swear_${userID}`);

                if (!swears) {
                    db.add(`swearCount_${userID}`, 1);
                    db.add(`globalSwear`, 1);
                } else {
                    db.add(`swearCount_${userID}`, 1);
                    db.add(`globalSwear`, 1);
                }

                return; // We quit
            }
        } catch (e) {
            console.log(`Error: ${e}`); // If error, don't stop bot, just log.
        }

    });

    if (msg.content === '->swear') {
        let userID = msg.author.id;
        let count = await db.fetch(`swearCount_${userID}`);
        let gcount = await db.fetch(`globalSwear`);
        let percentage = Math.floor((100 * count) / gcount);
        let msgcont = await db.fetch(`msgCount`);
        let totalPerc = Math.floor((100 * gcount) / msgcont);

        let embed = new Discord.RichEmbed()
            .addField(`Swear count for ${msg.author.username}:`, count ? count : "0")
            .addField(`Global swear count: `, gcount ? gcount : "0")
            .setDescription(`You said ${percentage ? percentage: 0}% of all the swears.`)
            .setFooter(`Out of ${msgcont} messages, ${totalPerc}% are swears.`);
        msg.channel.send(embed);
        return;
    }

    if (msg.content === '->ping') {
        msg.channel.send(`:ping_pong:**Pong!:** ${client.ping.toFixed()} ms`);
        return;
    }

    if (msg.content === '->about') {

        let gcount = await db.fetch(`globalSwear`);

        let embed = new Discord.RichEmbed()
            .setTitle("About OwU")
            .setDescription("OwU is a bot made by <@520317129677996032> and open-sourced on [GitLab](https://gitlab.com/JaeTheHyena/owo-uwu-bot) for the Discord Hacking Week. Feel free to modify it and do whatever you want with it.")
            .setAuthor("Jae", "", "https://jaekr.me")
            .setThumbnail("https://jaekr.me/logow.gif")
            .setFooter(`Total: ${gcount} swears.`);

        msg.channel.send(embed);

        return;
    }

    if (msg.content === '->help') {

        let embed = new Discord.RichEmbed()
            .addField('->help', "Show this page.", true)
            .addField('->swear', "Show the total number of swears and your total.", true)
            .addField('->about', "Some infos about the bot.", true)
            .setTimestamp();

        msg.channel.send(embed);

    }

});

client.login(config.token); // Login with the bot token